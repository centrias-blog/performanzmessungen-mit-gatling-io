var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "162",
        "ok": "162",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "359",
        "ok": "359",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "64",
        "ok": "64",
        "ko": "-"
    },
    "percentiles1": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "percentiles2": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "percentiles3": {
        "total": "352",
        "ok": "352",
        "ko": "-"
    },
    "percentiles4": {
        "total": "358",
        "ok": "358",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 12,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.218",
        "ok": "0.218",
        "ko": "-"
    }
},
contents: {
"req_https---www-ama-20d45": {
        type: "REQUEST",
        name: "https://www.amazon.de/",
path: "https://www.amazon.de/",
pathFormatted: "req_https---www-ama-20d45",
stats: {
    "name": "https://www.amazon.de/",
    "numberOfRequests": {
        "total": "4",
        "ok": "4",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "303",
        "ok": "303",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "359",
        "ok": "359",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "333",
        "ok": "333",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles1": {
        "total": "334",
        "ok": "334",
        "ko": "-"
    },
    "percentiles2": {
        "total": "348",
        "ok": "348",
        "ko": "-"
    },
    "percentiles3": {
        "total": "357",
        "ok": "357",
        "ko": "-"
    },
    "percentiles4": {
        "total": "359",
        "ok": "359",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.073",
        "ok": "0.073",
        "ko": "-"
    }
}
    },"req_https---www-goo-a053b": {
        type: "REQUEST",
        name: "https://www.google.de/",
path: "https://www.google.de/",
pathFormatted: "req_https---www-goo-a053b",
stats: {
    "name": "https://www.google.de/",
    "numberOfRequests": {
        "total": "4",
        "ok": "4",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "162",
        "ok": "162",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles1": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "270",
        "ok": "270",
        "ko": "-"
    },
    "percentiles4": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.073",
        "ok": "0.073",
        "ko": "-"
    }
}
    },"req_https---twitter-a629d": {
        type: "REQUEST",
        name: "https://twitter.com/home",
path: "https://twitter.com/home",
pathFormatted: "req_https---twitter-a629d",
stats: {
    "name": "https://twitter.com/home",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "percentiles1": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles2": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.036",
        "ok": "0.036",
        "ko": "-"
    }
}
    },"req_https---twitter-7ef3b": {
        type: "REQUEST",
        name: "https://twitter.com/home Redirect 1",
path: "https://twitter.com/home Redirect 1",
pathFormatted: "req_https---twitter-7ef3b",
stats: {
    "name": "https://twitter.com/home Redirect 1",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "347",
        "ok": "347",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "298",
        "ok": "298",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles1": {
        "total": "298",
        "ok": "298",
        "ko": "-"
    },
    "percentiles2": {
        "total": "323",
        "ok": "323",
        "ko": "-"
    },
    "percentiles3": {
        "total": "342",
        "ok": "342",
        "ko": "-"
    },
    "percentiles4": {
        "total": "346",
        "ok": "346",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.036",
        "ok": "0.036",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
