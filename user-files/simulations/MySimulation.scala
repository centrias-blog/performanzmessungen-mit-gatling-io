// package ccsample.gatling.simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class MeinErsterTest extends Simulation {

  val httpProtocol = http
      .baseUrl("http://127.0.0.1")
      .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
      .doNotTrackHeader("1")
      .acceptLanguageHeader("en-US,en;q=0.5")
      .acceptEncodingHeader("gzip, deflate")
      .userAgentHeader("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")

  val scn1 = scenario("Scenario1")
    .exec(Crawl.crawl)

  val userCount = Integer.getInteger("users", 1)
  val durationInSeconds  = java.lang.Long.getLong("duration", 10L)
  setUp(
    scn1.inject(rampUsers(userCount) during  (durationInSeconds seconds))
  ).protocols(httpProtocol)
}

object Crawl {

  val feeder = csv("/opt/gatling/user-files/test-url.csv").random

  val crawl = exec(feed(feeder)
    .exec(http("${testurl}")
    .get("${testurl}")
    ))
}
